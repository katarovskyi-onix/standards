\section{Spring Boot}\label{sec:spring-boot}
We use \href{https://spring.io/projects/spring-boot}{Spring Boot} as a standard framework for web applications development 

\subsection{Start a new project}\label{subsec:starting-new-project}
We use \href{https://start.spring.io/}{Spring Initializr} for starting new Spring Boot projects.
Creating your application with the Initializr ensures that you are picking up the tested and approved dependencies that will work well with Spring auto-configuration.

\subsection{Code structure}\label{subsec:code-structure}
We do not use default packages, we follow Java's recommendation about package naming conventions and use a reversed domain name (for example, com.onix.project).

Typical Spring Boot application layout:
\begin{lstlisting}
src/
    main/
        java/
            com/onix/project/
                Application.java
                config/SomeConfiguration.java
                entity/Product.java
                controller/ProductController.java
                repository/ProductRepository.java
                service/ProductService.java
        resources/application.yml
    test/
        java/
            com/onix/project/
            ApplicationTest.java
            entity/ProductTest.java
            controller/ProductControllerTest.java
            repository/ProductRepositoryTest.java
            service/ProductServiceTest.java
        resources/
\end{lstlisting}

We keep our Application.java (entry Class) in the top-level source directory.
The Application.java file should contain class with @SpringBootApplication annotation and main method:
\begin{lstlisting}[language=Java]
    package com.onix.project;

    import org.springframework.boot.SpringApplication;
    import org.springframework.boot.autoconfigure.SpringBootApplication;

    @SpringBootApplication
    public class Application {

        public static void main(String[] args) {
            SpringApplication.run(Application.class, args);
        }
    }
\end{lstlisting}

\subsection{Configuration}\label{subsec:configuration}
For application configuration we use Spring's Java configuration and do not use Spring's XML configuration.
We put all @Configuration classes with bean objects(@Bean annotation) to config package.
For example:
\begin{lstlisting}[language=Java]
    package com.onix.project.config;

    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;

    @Configuration
    public class SomeConfiguration {

        @Bean
        public SomeBean someBean() {
            return new SomeBean();
        }

    }
\end{lstlisting}

Also, we prefer to use yaml format instead .properties

\subsection{Application Build}\label{subsec:application-build}
We use \href{https://maven.apache.org/}{Maven} build tool and \href{http://tomcat.apache.org/}{Tomcat} servlets container, so we need to build java sources to WAR file.
With Spring Boot we follow the next rules:
\begin{itemize}
    \item Extend SpringBootServletInitializer.
    \begin{lstlisting}[language=Java]
    import org.springframework.boot.SpringApplication;
    import org.springframework.boot.autoconfigure.SpringBootApplication;
    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

    @SpringBootApplication
    public class Application extends SpringBootServletInitializer {

        public static void main(String[] args) {
            SpringApplication.run(Application.class, args);
        }

        @Override
        protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
            return builder.sources(Application.class);
        }
    }
    \end{lstlisting}
    \item Mark the embedded servlet container as provided.
    \begin{lstlisting}
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-tomcat</artifactId>
        <scope>provided</scope>
    </dependency>
    \end{lstlisting}
    \item Update packaging to war in pom.xml.
    \begin{lstlisting}
    <packaging>war</packaging>
    \end{lstlisting}
\end{itemize}

\subsection{Best practices}\label{subsec:spring-boot-best-practices}
\begin{itemize}
    \item Use Spring Boot auto-configuration.
    \item Controllers are supposed to be clean and lightweight.
    \item @Service annotation class for business logic.
    \item We do not use Spring Boot code inside domain classes.
    \item Use constructor-based injection.
\end{itemize}

\subsection{Security}\label{subsec:security}

\subsubsection{HTTPS in production}
Very important to use HTTPS in Spring Boot application.
The next @Configuration class requires a secure connection:
\begin{lstlisting}[language=Java]
    @Configuration
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.requiresChannel().requiresSecure();
        }
    }
\end{lstlisting}

\subsubsection{Test included dependencies}
Need to ensure that application does not use dependencies with known vulnerabilities.
We use a tool like \href{https://snyk.io/}{Snyk} to:
\begin{itemize}
    \item Test application dependencies for known vulnerabilities.
    \item Automatically fix problems that exist.
    \item Continuously monitor for new vulnerabilities.
\end{itemize}

\subsubsection{CSRF Protection}
Spring Security enables CSRF support by default.
If you use a JavaScript framework, configure the CookieCsrfTokenRepository so cookies are not HTTP-only:
\begin{lstlisting}[language=Java]
    @EnableWebSecurity
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        }
    }
\end{lstlisting}

\subsubsection{Content Security Policy(CSP)}
Enable to avoid XSS attacks.
Spring Security provides a number of security headers by default, but not CSP.
Enable \href{https://docs.spring.io/spring-security/site/docs/5.2.0.BUILD-SNAPSHOT/reference/htmlsingle/#headers-csp-configure}{Spring CSP} in the next way:
\begin{lstlisting}[language=Java]
    @EnableWebSecurity
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.headers().contentSecurityPolicy("script-src 'self' https://trustedscripts.example.com; object-src https://trustedplugins.example.com; report-uri /csp-report-endpoint/");
        }
    }
\end{lstlisting}

\subsubsection{OpenID connect}
\href{https://en.wikipedia.org/wiki/OpenID_Connect}{OpenID Connect (OIDC)} is an OAuth 2.0 extension that provides user information via an ID token in addition to an access token.
Query the /userinfo endpoint for retrieve user information.

\subsubsection{Password hashing}
We do not store passwords in plain text and Spring Security doesn't support plain text passwords by default.
We use \href{https://docs.spring.io/spring-security/site/docs/current/api/org/springframework/security/crypto/bcrypt/BCryptPasswordEncoder.html}{BCrypt} implementation for password hashing:
\begin{lstlisting}[language=Java]
    @EnableWebSecurity
    public class WebSecurityConfig {
        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }
    }
\end{lstlisting}

\subsubsection{Use the Latest Releases}
The \href{start.spring.io}{start.spring.io} generator automatically uses the latest versions of dependencies.
For existing applications, when upgrades aren't possible, consider patches from a security vendor, like \href{https://snyk.io/}{Snyk}.

\subsubsection{Spring Vault}
\href{https://www.vaultproject.io/}{HashiCorp Vault} provides secure storage for sensitive information like passwords, tokens etc.
\href{https://projects.spring.io/spring-vault/}{Spring Vault} provides spimple Spring abstractions 
and client-side support for accessing, storing, and revoking secrets.

\subsubsection{Security testing}
\begin{itemize}
    \item Security team code review.
    \item Automatic testing using \href{https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project}{OWASP ZAP}
\end{itemize}