\section{Introduction}
\subsection{Project Setup}
Before launching the application, you need to install
dependencies(\texttt{pod\ install}), because they are not stored in the
repository.

\section{Managing dependencies}

\subsection{CocoaPods}

For all dependencies you need to add a version. Example:
\begin{lstlisting}
pod 'SwiftLint', '~> 0.38.0'
\end{lstlisting}

\subsection{Dependences}
\begin{itemize}
\item  \href{https://github.com/Quick/Nimble}{Nimble} and  \href{https://github.com/Quick/Nimble}{Quick} - The dependencies are for write tests in a declarative style. It simplifies the readability of the tests.

\item \href{https://firebase.google.com/docs/crashlytics}{Firebase/Analytics; Fabric; Crashlytics} - Analytics and crashlytics

\item \href{https://github.com/ninjaprox/NVActivityIndicatorView}{NVActivityIndicatorView} - Dependency displays the Activity indicator (loading process).

\item \href{https://github.com/realm/SwiftLint}{SwiftLint} - 
A tool to enforce code style and it isn't allowed to write code that may cause the app to crash, for example \texttt{force\ unwrapping}. All rules in the file \texttt{.swiftlint.yml}.

\item \href{https://github.com/a-voronov/swifty-redux}{SwiftyRedux} - In the app use the \href{https://redux.js.org/introduction/getting-started}{redux} architecture. A dependency that contains \texttt{core} for the architecture.

\item \href{https://github.com/krzysztofzablocki/Sourcery}{Sourcery} Dependency for code generation. Generate Lenses(AutoLenses) and Codable(AutoCodable):
    \begin{itemize}
    \item \href{http://chris.eidhof.nl/post/lenses-in-swift/}{Lenses} For convenient changes to \texttt{immutable} values.
    \item \href{https://developer.apple.com/documentation/swift/codable}{Codable} it is mainly used for \texttt{enum}
    \end{itemize}
\item \href{https://github.com/michaeltyson/TPKeyboardAvoiding}{TPKeyboardAvoidingSwift} Dependency for convenient keyboard operation.
\item \href{https://github.com/jrendel/SwiftKeychainWrapper}{SwiftKeychainWrapper} Dependency for convenient working with \href{https://developer.apple.com/documentation/security/keychain_services}{Keychain Services}
\item \href{https://github.com/mac-cain13/R.swift}{R.swift} A dependency that generates resources for images, fonts, strings, etc. For example: \texttt{R.image.settingsIcon()}.

\item \href{https://github.com/onevcat/Kingfisher}{Kingfisher} Dependency for loading / caching images.



\end{itemize}



\section{Development rules}

\subsection{Using dependencies}

Rule: dependency must be \texttt{import\ only\ once}. To perform this action
wrap the dependency to abstraction. It is necessary to avoid any difficulties
of transition from one dependency to another. 

Example with \href{https://github.com/onevcat/Kingfisher}{Kingfisher} without the
wrapper:

\begin{lstlisting}[language=Java]
import Kingfisher

class ViewController: UIViewController { 
    @IBOutlet private var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://example.com/image.png")
        imageView.kf.setImage(with: url)
    }
}
\end{lstlisting}

In this example the `ViewController` knows about the dependency that loads the image.
 
Example with \href{https://github.com/onevcat/Kingfisher}{Kingfisher} with the wrapper:
\begin{lstlisting}[language=Java]
import Kingfisher

extension UIImageView { 
    func setImage(by url: URL?) {
        imageView.kf.setImage(with: url)
    }
}
\end{lstlisting}

\begin{lstlisting}[language=Java]
class ViewController: UIViewController { 
    @IBOutlet private var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://example.com/image.png")
        imageView.setImage(with: url)
    }
}
\end{lstlisting}

There is a visible difference between two examples. And when we decide to stop using Kingfisher
we need to change loading/caching image implementation in `extension UIImageView` , and the other classes will not be affected.

\section{Resources}

\subsection{Localization} \label{Localization}

The application is localized using a file `L.swift`. All localized strings are added separately for each screen/functionality. Example:
\begin{lstlisting}[language=Java]
enum L { 
    // we use it in the app for models and any business logic.
    static let male = R.string.localizable.male() 

    enum RegistrationForm {
        static let male = R.string.localizable.male() // we use it in the RegistrationForm screen.
    }

    enum Profile {
        static let male = R.string.localizable.male() // we use it in the Profile screen.
    }
}
\end{lstlisting}

The same resource is used for this word and in this way the localization is divided into functionality. This approach makes it possible to easily change the localization on the place where we need it.


\section{Images} \label{Images}

All images are added/used using a file `I.swift`. If necessary, we divide usage in the same way as \autoref{Localization}.

\section{Fonts} \label{Fonts}

The token is passed to the:

\begin{lstlisting}[language=Java]
func fontBy(_ token: FontToken) -> UIFont
\end{lstlisting}

function for obtaining the fonts. All the tokens are in the \texttt{`FontToken`} file. Adding it, you also need to divide it into elements where the font is used. For example:


\begin{lstlisting}[language=Java]
enum FontToken { 
    case title04 // ``Montserrat-Bold'', size: 14
    case text16     // "Montserrat-Bold", size: 14
    case button04   // "Montserrat-Bold", size: 14
}
\end{lstlisting}

It is recommended to work in collaboration with designers, which allows you to easily change fonts for certain UI elements.


\section{Colors} \label{Colors}

This approach uses color schemes. The token is passed to the
\begin{lstlisting}[language=Java]
func colorBy(_ token: ColorToken) -> UIColor
\end{lstlisting}
function for obtaining the colors. All the tokens are in the \texttt{`ColorToken`} file. Adding it, you also need to divide it into elements where the color is used. For example: 

\begin{lstlisting}[language=Java]
switch token { 
    case .title02: return // \#07906B
    case .text03:
        return // #07906B
        
    case .button02:
        return // #07906B
        
    case .frame01:
        return // #07906B
        
    case .navigationText01:
        return // #07906B
    }
}
\end{lstlisting}

It is recommended to work in collaboration with designers, which allows you to easily change colors for certain UI elements and adding new color schemes.

\section{Constants}

All constants are added/used using the \texttt{`C.swift`} file. We divide it into logical components for convenient use and context.
 
We use the \texttt{`Defines.swift`} file to configure constants that have some logic. This example shows that we use one URL in `DEBUG` and another in `RELEASE`: 

\begin{lstlisting}[language=Java]
enum Defines { 
    enum Url { 
        enum Server { 
            static var baseURL: URL {
                #if DEBUG 
                return URL(string: C.URLs.Server.dev)! 
                #else 
                return URL(string: C.URLs.Server.prod)! 
                #endif
            }
        }
    }
}
\end{lstlisting}

\section{UI development}

On one `.storyboard` we develop a design for one screen. 
 
It is necessary to add the `func setupUI()` function for each UI (`UIView/UIViewController`) element. This function performs the following configurations:
\begin{itemize}
\item configure the localization as described in \autoref{Localization}
\item configure the static images as described in \autoref{Images}
\item configure the fonts as described in \autoref{Fonts}
\item configure the colors for the elements (`backgroundColor, textColor, etc`) as described in \autoref{Colors}
\end{itemize}

This approach lengthens the process of developing UI elements about 5-10 percent however it makes our code more flexible to change.
 
\section{Architecture}

Unidirectional architecture \href{https://redux.js.org/introduction/getting-started}{redux} contains the main components: 

\begin{itemize}
\item \texttt{State} is a single data structure. The state can be `struct` or `enum`. The state may contain other states which describe the entire state tree of the application and eliminates conflicting states. The state must be easily serialized and deserialized, so it should conform to the `Equatable` protocol. Also, it doesn't have to contain any business logic.
 
\item \texttt{Store} is a place that stores \texttt{State}, allows you to subscribe/unsubscribe to changes on the `state`. Also it is responsible for delivery\texttt{Action} to\texttt{Reducer}, notifies about \texttt{State} changes. Each app can have one \texttt{Store}. 
 
\item \texttt{Action} is an object describes the logic of what is happening. All these objects must conform to the `Action` protocol. 
The only way to change \texttt{State} is to send an \texttt{Action} to \texttt{Store} using the `store.dispatch()` function. \texttt{Action} will be processed by the \texttt{Reducer}.
 
\item \texttt{Reducer} is a place that can change \texttt{State} based on the current\texttt{State} and\texttt{Action}. Reducer is a pure function. You can't perform any side effects, you can't call unclean functions, for example `Date()` or `Int.random(in: 0...10)`.
 
\item \texttt{Middleware} is a place for asynchronous logic (accessing the server, etc.) and business logic. In this place all\texttt{Action} come before they get to \texttt{Reducer}, this way, you can change the behavior here like interrupting actions and changing them.
 
\item \texttt{ActionCreator} is a place for asynchronous logic (accessing the server, etc.) and business logic. It can perform some query/logic and generate an action to send to the \texttt{Store}.
 
\end{itemize}

\begin{figure}
\includegraphics[width=0.9\textwidth]{redux.png}
\caption{Redux flow diagram}
\label{redux_flow}
\end{figure}

\section{Navigation}

In this case, we should use the \href{https://www.raywenderlich.com/158-coordinator-tutorial-for-ios-getting-started}{Coordinator} approach. 

Coordinator is an object that is responsible for switching between screens or other coordinators. It needs to have access to the screen factory or other coordinator. Contains \texttt{enum Route} which describes all possible transitions. All transitions are performed by the \texttt{func route(\_ route: Route)} function. It can also contain \texttt{struct Output} to leave this coordinator or notify about the completion of some work. `AuthorizationCoordinator.swift` is such an example. 

\begin{lstlisting}[language=Java]
extension AuthorizationCoordinator {
    struct Output {
        let finished: Command        
        static let initial = Output(finished: .nop)
    }
}
 
extension AuthorizationCoordinator {
    private enum Route {
        case loginPresent
        case loginDismiss
    }
}
 
final class AuthorizationCoordinator: Coordinator {
    private let window: UIWindow
    private let output: Output
    private weak var rootController: UIViewController?
    
    init(window: UIWindow, output: Output = .initial) {
        self.window = window
        self.output = output
    }
    
    func start() {
        let rootController = self.accountInit()
        self.window.rootViewController = rootController
        self.rootController = rootController
    }
    
    private func route(_ route: Route) {
        switch route {
        case .loginPresent:
            rootController?.present(login, animated: true)
        case .loginDismiss:
            login.dismiss(animated: true)
        }
    }
    
    private func didLoginFinished() {
        login.dismiss(animated: false)
        self.output.finished.perform()
    }
    
    private func accountInit() -> AccountInitViewController {
        let output = AccountInitFactory.Output(
            login: Command { [weak self] in
                self?.route(.loginPresent)
            },
            signUp: Command { [weak self] in
                self?.route(.signUpPresent)
            }
        )
        return AccountInitFactory().default(output)
    }
    
    private lazy var login: LoginViewController = {
        let output = LoginFactory.Output(
            close: Command { [weak self] in
                self?.route(.loginDismiss)
            },
            didLogin: Command { [weak self] in
                self?.didLoginFinished()
            }
        )
        return LoginFactory().default(output)
    }()
}
\end{lstlisting}

\section{Development}

Each screen should be developed as a module. \texttt{The main rule}: the controller must be able to rise as a root screen on \texttt{window}, no matter what architecture is the app been developed on.
 
The \href{https://redux.js.org/introduction/getting-started}{redux} architecture consist of following components:

\subsection{View}

\texttt{(UIView or UIViewController)} contains \texttt{Props}. \texttt{Props} is only one way to communicate/build the \texttt{View}. Props - is the object describes the \texttt{`view`} and all possible interaction with this view (display title/image/list, click on a button, etc.). Props can contain subprops of elements that are on this \texttt{`view`}. For example, there is a table on the screen and the props should contain an array of cell props. Each Props must have \texttt{`static let initial: Props`} for an empty implementation to avoid optional variables and it must conform to the protocol `Equatable`, `AutoCodable`. Example: 

\begin{lstlisting}[language=Java]
extension SearchProductViewController.Props: Equatable, AutoCodable {}
extension SearchProductViewController.Props.State: Equatable, AutoCodable {}
final class SearchProductViewController: UIViewController {
    struct Props {
        let foundCount: Int
        let state: State; enum State {
            case initial
            case loading
            case failure(String)
        }
        
        let items: [DiscountProductCell.Props]
        let search: CommandWith<String?>
        let onClose: Command
        let onDestroy: Command
        
        static let initial: Props = .init(
            foundCount: 0,
            state: .initial,
            items: [],
            search: .nop,
            loadMore: .nop,
            onClose: .nop,
            onDestroy: .nop
        )
    }
}
\end{lstlisting}

\subsection{Presenter}

\texttt{Presenter} is a structure (`struct`) collecting \texttt{Props} from \texttt{State} to \texttt{View}. It doesn't have to perform any business logic or asynchronous operations, but only convert the \texttt{State} to the \texttt{Props} and broadcast events to a higher level. For Example: 

\begin{lstlisting}[language=Java]
struct SearchProductPresenter {
    typealias Props = SearchProductViewController.Props
    
    let render: CommandWith<Props>
    let dispatch: CommandWith<Action>
    let search: CommandWith<String?>
    let onProduct: CommandWith<Product>
    let onClose: Command
    let endObserving: Command
    
    func present(state: AppState) {
        render.perform(
            with: .init(
                foundCount: state.searchProduct.pagination.count,
                state: state |> propsState,
                items: state |> itemsProps,
                search: search,
                onClose: onClose,
                onDestroy: endObserving
            )
        )
    }
    
    private func propsState(_ state: AppState) -> Props.State {
        switch state.searchProduct.state {
        case .initial, .loaded:
            return .initial
        case .loading:
            return .loading
        case .failed(let message):
            return .failure(message)
        }
    }
    
    private func itemsProps(_ state: AppState) -> [DiscountProductCell.Props] {
        return state.searchProduct.products.map { product in
            .init(
                imageURL: product.imagesURL.first,
                title: product.title,
                oldPrice: "\(product.oldPrice)",
                newPrice: "\(product.newPrice)",
                date: product.promotionDate(),
                percentDiscount: product.percentDiscount(),
                onSelect: Command {
                    self.onProduct.perform(with: product)
                }
            )
        }
    }
}
\end{lstlisting}

\subsection{ScreenFactory}

\texttt{ScreenFactory} is an object that returns \texttt{UIViewController}. It binds elements to interact:

\begin{itemize}
\item subscribe / unsubscribe to change \texttt{State}
\item binds \texttt{View} and \texttt{Presenter} for configuring \texttt{Props} and rendering on the screen.
\item binds \texttt{Presenter} to other objects responsible for business logic (ActionCreator, formatters, etc.).
\end{itemize}

\begin{lstlisting}[language=Java]
extension SearchProductScreenFactory {
    struct Output {
        let product: CommandWith<Product>
        let close: Command
        
        static let initial = Output(product: .nop, close: .nop)
    }
}
 
final class SearchProductScreenFactory {
    private let store: Store<AppState>
    
    init(store: Store<AppState> = StoreLocator.shared) {
        self.store = store
    }
    
    func `default`(
        discountID: Int? = nil,
        _ output: Output = .initial
    ) -> SearchProductViewController {
        let controller: SearchProductViewController = Storyboard.main.searchProduct.instantiate()
        var disposable: Disposable?
        
        let dispatcher = CommandWith { [weak store] in store?.dispatch($0) }
        let endObserving = Command {
            disposable?.dispose()
            SearchProductClearAction() |> dispatcher.perform
        }
        let render = CommandWith { [weak controller] props in
            controller?.render(props)
        }
        let searchProducts = SearchProductsLoader(by: discountID, dispatch: dispatcher) { [weak store] in
            store?.state
        }
            
        let presenter = SearchProductPresenter(
            render: render.dispatched(on: .main),
            dispatch: dispatcher,
            search: CommandWith { text in
                searchProducts.search(text)
            },
            onProduct: output.product,
            onClose: output.close,
            endObserving: endObserving
        )
        disposable = store.subscribeUnique(observer: presenter.present)
        return controller
    }
    
    deinit {
        print("Deinit " + String(describing: type(of: self)))
    }
}
\end{lstlisting}

\begin{itemize}
\item \texttt{struct Output} notifies the coordinator about actions on the screen (selecting an item to switch to another screen/closing the screen, etc.).

  \begin{itemize}
  \item \texttt{func default} is a function that returns the prepared screen for use in the application. The factory can contain a lot of screen configuration functions, for different requirements (different actionCreator, formatters, etc.):

    \begin{itemize}
    \item
      \texttt{controller} is the UI element.
    \item
      \texttt{disposable} is variable to unsubscribe from \texttt{State} updates. In configuration function we can manage the subscription/unsubscription to the \texttt{State} changes.
    \item
      \texttt{dispatcher} broadcasts action to the store.
    \item
      \texttt{endObserving} is a notification wich is used to inform the Factory to unsubscribe from the \texttt{State} updates. The main function `endObserving` is to unsubscribe(`disposable?.dispose()`) from the \texttt{State} updates. In addition it could perform additional some actions, such as sending an action to clear the State.
    \item
      \texttt{render} is variable that we pass to \texttt{Presenter}. It receives the `Props` and passes them to the screen for rendering.
    \item
      \texttt{searchProducts} is an example of \texttt{ActionCreator}. In this case, it receives text, checks that, and sends a request to the server.
    \item
      \texttt{presenter} is variable to bind all objects together.
    \end{itemize}
  \end{itemize}
\end{itemize}

This link \href{https://drive.google.com/open?id=1sTRVVxBE8E_CKWM8kL7yaKkfQS_-iZ70}{redux module template} exists for convenient development of the module. This template creates all elements containing the basic implementation for the screen, such as \texttt{ScreenFactory}, \texttt{Presenter}, \texttt{ViewController}, \texttt{Storyboard}. \href{https://blog.kulman.sk/creating-your-own-xcode-templates/}{Creating and using templates}.

\section{Testing}

There are a few main components for testing, such as \texttt{Reducer}, \texttt{Presenter}, \texttt{ActionCreator}. They should be easily covered with tests. If the test is difficult to write, then the component is implemented incorrectly and needs to be refactored. \href{https://redux.js.org/introduction/getting-started}{Guide for example}

\section{Switching from Redux architecture to a different one}

We design screens as a module so we can easily move from the Redux architecture to any convenient one. For example, we can use the same approach with \texttt{ScreenFactory}, and instead of working with the `State`, you can refer to different services using \texttt{ServiceHolder}.
