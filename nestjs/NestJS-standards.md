# Contents:
- [Code Style](#code-style)
    - [Base style](#base-style)
    - [Design Best Practices](#design-best-practices)
- [Libraries](#libraries)
- [Tools](#tools)
    - [IDE](#ide)
    - [Code Quality](#code-quality)
    - [Migration tools](#migration-tools)
    - [Code coverage](#code-coverage)
- [NestJS](#nestjs)
    - [Prerequisites](#prerequisites)
    - [Setup](#setup)
    - [Project structure](#project-structure)
    - [NestJS units](#nestjs-units)
    - [Fundamentals](#fundamentals)
    - [Testing](#testing)
# Code Style
- ## Base style:
    > In general, our TypeScript code style is based on the Airbnb Style but with some differences.
    
    > Add to `.eslintrc.json`
    ```JSON
    { "rules": {
           "no-shadow": "error",
           "no-useless-return": "error",
           "newline-before-return": "error",
           "prefer-object-spread": "error",
           "arrow-body-style": [
               "error",
               "as-needed"
           ],
           "newline-per-chained-call": "error",
           "no-duplicate-imports": [
               "error",
               {
                   "includeExports": true
               }
           ],
           "semi": "off",
           "indent": "off",
           "quotes": [
               "error",
               "single"
           ],
           "max-len": [
               "error",
               {
                   "code": 150
               }
           ],
           "comma-dangle": [
               "error",
               {
                   "arrays": "never",
                   "functions": "never"
               }
           ],
           "@typescript-eslint/no-explicit-any": "error",
           "@typescript-eslint/require-await": "off",
           "@typescript-eslint/typedef": [
               "error",
               {
                   "arrayDestructuring": true,
                   "arrowParameter": true,
                   "variableDeclaration": false
               }
           ],
           "@typescript-eslint/class-name-casing": "error",
           "@typescript-eslint/semi": [
               "error"
           ],
           "@typescript-eslint/camelcase": "off",
           "@typescript-eslint/unbound-method": [
               "off"
           ],
           "no-unused-vars": "off",
           "@typescript-eslint/no-unused-vars": [
               "error",
               {
                   "vars": "all",
                   "args": "after-used",
                   "ignoreRestSiblings": false
               }
           ]
       }
    }
    ```
    > We use eslint:recommended config with typescript-eslint plugins
    for VSCode.
    ```JSON
    { "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:security/recommended"
      ]
    }
    ```
    > Also you can follow unofficial TypeScript [StyleGuide](https://github.com/basarat/typescript-book/blob/master/docs/styleguide/styleguide.md)

- ## Design Best Practices
    - #### [Don’t repeat yourself](https://www.taniarascia.com/refactoring-dont-repeat-yourself/)
    - #### [KISS](https://medium.com/infancyit/kiss-keep-it-simple-smart-javascript-habits-b019ce14b5ec)
    - #### [SOLID](https://medium.com/@cramirez92/s-o-l-i-d-the-first-5-priciples-of-object-oriented-design-with-javascript-790f6ac9b9fa)

# Libraries
> Most used libraries with any NodeJS project    
* [Nvm]()
* [NodeJS](#NodeJS)
* [Git](##Git)
* [PostgreSQL](##PostgreSQL)
* [MongoDB](##MongoDB)
* [TypeORM](##TypeORM)
* [Mongoose](##Mongoose)
* [Pg](##Pg)
* [Typescript](##Typescript)
* [Nodemon](##Nodemon)
* [Bluebird](##Bluebird)
* [PassportJS](##PassportJS)
* [Swagger](##Swagger)
* [MomentJS](##MomentJS)
* [Lodash](##Lodash)
* [Jest](##Jest)
* [ExpressJS](##ExpressJS)
* [Axios](##Axios)
* [Dotenv](##Dotenv)
* [Winston](##Winston)
* [Docker](##Docker)
* [Docker-compose](##docker-compose)

- ## [NodeJS](https://nodejs.org/en/about/)
`$ nvm install latest` - will install the latest nodejs version to your system
> As an asynchronous event-driven JavaScript runtime, Node.js is designed to build scalable network applications. In the following "hello world" example, many connections can be handled concurrently. Upon each connection, the callback is fired, but if there is no work to be done, Node.js will sleep.

- ## [PostgreSQL](https://www.postgresqltutorial.com/install-postgresql/)
> PostgreSQL is a powerful, open source object-relational database system.
> install PostgreSQL follow [this](https://www.postgresqltutorial.com/install-postgresql/) tutorial.

- ## [Git](https://www.npmjs.com/package/git) 
`$ brew install git`
> Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows.

- ## [MongoDB](https://www.mongodb.com/)
`$ npm install mongodb --save`
> The official MongoDB driver for Node.js. Provides a high-level API on top of mongodb-core that is meant for end users.

- ## [TypeORM](http://typeorm.io/)
`$ npm i --save @nestjs/typeorm typeorm`
> TypeORM module for Nest.

- ## [Mongoose](http://mongoosejs.com/)
`$ npm i --save @nestjs/mongoose mongoose`
> Mongoose module for Nest.

- ## [Pg](https://www.npmjs.com/package/pg)	
`$ npm install pg --save`
> Non-blocking PostgreSQL client for Node.js. Pure JavaScript and optional native libpq bindings.

- ## [Typescript](https://www.typescriptlang.org/)
`$ npm install -g typescript`
> TypeScript is a language for application-scale JavaScript

- ## [Nodemon](https://www.npmjs.com/package/nodemon)
`$ npm install -g nodemon`
> Nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.

- ## [Bluebird](http://bluebirdjs.com/docs/getting-started.html)
`$ npm install --save bluebird`
> Bluebird is a fully featured promise library with focus on innovative features and performance

- ## [PassportJS](http://www.passportjs.org/)
`$ npm i --save @nestjs/passport passport`
> Passport utilities module for Nest.

- ## [Swagger](https://www.openapis.org/)
`$ npm i --save @nestjs/swagger`
> OpenAPI (Swagger) module for Nest.

- ## [MomentJS](https://momentjs.com/)
`$ npm i --save moment`
> A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.

- ## [Lodash](https://lodash.com/)
`$ npm i --save lodash`
> The Lodash library exported as Node.js modules.

- ## [Jest](https://jestjs.io/)
`$ npm i --save-dev jest`
> Delightful JavaScript Testing

- ## [ExpressJS](https://expressjs.com/)
`$ npm i --save express`
> Fast, unopinionated, minimalist web framework for node.

- ## [Axios](https://github.com/axios/axios)
`$ npm i --save express`
> Promise based HTTP client for the browser and node.js

- ## [Dotenv](https://www.npmjs.com/package/dotenv)
`$ npm i --save dotenv`
> Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env

- ## [Winston](https://www.npmjs.com/package/winston)
`$ npm i --save winston`
> A logger for just about everything.

- ## [Docker](https://www.docker.com/)
> how to [start](https://www.docker.com/get-started)

- ## [Docker-compose](https://docs.docker.com/compose/)
> how to [install](https://docs.docker.com/compose/install/) and use 

# Tools
- ## IDE
    - [Visual Studio Code](https://code.visualstudio.com/)
    > (We use VSCode. It has a ton of awesome features and additional plugins for everything and it is free).
    - [WebStorm](https://www.jetbrains.com/webstorm/) (Is powerful IDE but it is paid)
    - [Sublime](https://www.sublimetext.com/)
    - [Atom](https://atom.io/)
- ## Code Quality
    - [Onix-systems GitLab](https://gitlab.onix.ua/)
    - [Sonar-Qube](https://www.sonarqube.org/)
    - [EsLint](https://eslint.org/docs/user-guide/configuring)
- ## Migration tools
    > We use TypeORM migrations https://typeorm.io/#/migrations in NestJS for SQL DB’s. But you can use any other libraries for your specific DB.
- ## Code coverage
    - [Jest](##Jest)
    > Jest is a delightful JavaScript Testing Framework with a focus on simplicity.

# [NestJS](https://docs.nestjs.com/)
- ## Prerequisites.
    > Please make sure that Node.js (>= 10.13.0) is installed on your operating system.
- ## Setup
    > Setting up a new project is quite simple with the [Nest CLI](https://docs.nestjs.com/cli/overview). With npm installed, you can create a new Nest project with the following commands in your OS terminal:

    ```
    $ npm install -g @nestjs/cli
    ```
    
    > To create, build and run a new basic Nest project in development mode, go to the folder that should be the parent of your new project, and run the following commands:
    
    ```
    $ nest new my-nest-project
    $ cd my-nest-project
    $ npm run start:dev
    ```
    
    > In your browser, open http://localhost:3000 to see the new application running. The app will automatically recompile and reload when you change any of the source files.

- ## Project structure
    > When you run `nest new`, Nest generates a boilerplate application structure by creating a new folder and populating an initial set of files. You can continue working in this default structure, adding new components, as described throughout this documentation. 
    
    ```
    project_folder
    ├── dist
    ├── node_modules
    │   ├── module
    │   ├── ...
    ├── src
    │   └── app
    │       ├── authentication
    │       │       ├── controller.ts
    │       │       ├── service.ts
    │       │       ├── module.ts
    │       │       ├── repository.ts
    │       │       ├── pipes
    │       │       │   └── authentication-validation.pipe.ts
    │       │       ├── interfaces
    │       │       │   └── authentication.interface.ts
    │       │       └── dto
    │       │       │   ├── sign-up.dto.ts
    │       │       │   └── sign-in.dto.ts
    │       │       └── tests
    │       │           ├── controller.spec.ts
    │       │           ├── repository.spec.ts
    │       │           └── service.spec.ts
    │       ├── user
    │       │   └── components
    │       │       ├── controller.ts
    │       │       ├── service.ts
    │       │       ├── module.ts
    │       │       ├── repository.ts
    │       │       ├── pipes
    │       │       │   └── user-validation.pipe.ts
    │       │       ├── interfaces
    │       │       │   └── user.interface.ts
    │       │       └── dto
    │       │       │   ├── delete-user.dto.ts
    │       │       │   └── create-user.dto.ts
    │       │       └── tests
    │       │           ├── controller.spec.ts
    │       │           ├── repository.spec.ts
    │       │           └── service.spec.ts
    │       ├── app.module.ts
    │       └── main.ts      
    ├── shared
    │   ├── credentials
    │   ├── documentation
    │   │   └── swaggerFile.ts
    │   ├── i18n
    │   │   └── en
    │   │   └── ua
    │   ├── seeds
    │   │   └── fillUsers.ts
    │   ├── migrations
    │   │   └── addNewColumn.ts
    │   └── componentName
    │       ├── service.ts
    │       ├── module.ts
    │       ├── repository.ts
    │       ├── pipe.ts
    │       ├── interface.ts
    │       └── dto.ts
    ├── .env
    ├── .eslintrc.json
    ├── .gitignore
    ├── nest-cli.json
    ├── nodemon.json
    ├── package-lock.json
    ├── package.json
    ├── README.md
    ├── tsconfig.build.json
    └── tsconfig.json
    ```

- ## NestJS units
    - #### [Controllers](https://docs.nestjs.com/controllers)
    > In Nestjs controllers are responsible for handling incoming requests and 
    returning responses to the client. They are defined using the @Controller() 
    declarator which takes the path for the primary route as its argument. 
    ![](https://cms.gabrieltanner.org/content/images/2019/09/Nestjs-controllers.jpg)
    
    > Each function inside the controller can be annotated with the following 
    declarators:
    - @Get() — Defines a get request
    - @Post() — Defines a post request
    - @Delete() — Delete request
    - @Put() — Put request
    - Here is an example of a simple controller with one get route:
    
    `cats.controller.ts`
    ```typescript
    import { Controller, Get, Query, Post, Body, Put, Param, Delete } from '@nestjs/common';
    import { CreateCatDto, UpdateCatDto, ListAllEntities } from './dto';

    @Controller('cats')
    export class CatsController {
      @Post()
      create(@Body() createCatDto: CreateCatDto) {
        return 'This action adds a new cat';
      }

      @Get()
      findAll(@Query() query: ListAllEntities) {
        return `This action returns all cats (limit: ${query.limit} items)`;
      }

      @Get(':id')
      findOne(@Param('id') id: string) {
        return `This action returns a #${id} cat`;
      }

      @Put(':id')
      update(@Param('id') id: string, @Body() updateCatDto: UpdateCatDto) {
        return `This action updates a #${id} cat`;
      }

      @Delete(':id')
      remove(@Param('id') id: string) {
        return `This action removes a #${id} cat`;
      }
    }

    ```
    > Note: After creating the controller it needs to be added to a module so 
    Nestjs can recognize it (This happens automatically when you generate it 
    using the Nest CLI).

    - #### [Providers](https://docs.nestjs.com/providers)
    > Providers in Nestjs also referred to as services are used to encapsulate
    and abstract the logic of other classes like controllers. They can be 
    injected into other classes using dependency injection.
    ![](https://cms.gabrieltanner.org/content/images/2019/09/Nestjs-providers.jpg)
    
    > A provider is a normal Typescript class with an @Injectable() declarator on the top.
    >
    > For example, we can easily create a service which fetches all our items and use it in our ItemController.
    
    `cats.service.ts`
    ```typescript
    import { Injectable } from '@nestjs/common';
    import { Cat } from './interfaces/cat.interface';

    @Injectable()
    export class CatsService {
      private readonly cats: Cat[] = [];

      create(cat: Cat) {
        this.cats.push(cat);
      }

      findAll(): Cat[] {
        return this.cats;
      }
    }   
    ```

    - #### [Modules](https://docs.nestjs.com/modules)
    > A module is a class annotated with a @Module() decorator. The @Module() 
    > decorator provides metadata that Nest makes use of to organize the 
    > application structure.
    >![](https://docs.nestjs.com/assets/Modules_1.png)
                                          
    > Each application has at least one module, a root module. The root module is the starting point Nest uses to build the application graph - the internal data structure Nest uses to resolve module and provider relationships and dependencies.
 
    `cats.module.ts`
    ```typescript
    import { Module } from '@nestjs/common';
    import { CatsController } from './cats.controller';
    import { CatsService } from './cats.service';

    @Module({
        controllers: [CatsController],
        providers: [CatsService],
        exports: [CatsService]
    })
    export class CatsModule {}
    ```

    - #### [Middleware](https://docs.nestjs.com/middleware)
    > Middleware is a function which is called before the route handler. 
    Middleware functions have access to the request and response objects, and 
    the next() middleware function in the application’s request-response cycle. 
    The next middleware function is commonly denoted by a variable named next.
    ![](https://docs.nestjs.com/assets/Middlewares_1.png)
                                                   
    > Nest middleware are, by default, equivalent to express middleware.
    You implement custom Nest middleware in either a function, or in a class
    with an @Injectable() decorator. The class should implement the 
    NestMiddleware interface, while the function does not have any special 
    requirements. Let's start by implementing a simple middleware feature using 
    the class method.

    `logger.middleware.ts`
    ```typescript
    import { Injectable, NestMiddleware } from '@nestjs/common';
    import { Request, Response } from 'express';

    @Injectable()
    export class LoggerMiddleware implements NestMiddleware {
      use(req: Request, res: Response, next: Function) {
        console.log('Request...');
        next();
      }
    }
    ```

    * ### [Exception filters](https://docs.nestjs.com/exception-filters)
    > Nest comes with a built-in exceptions layer which is responsible for processing all unhandled exceptions across an application. When an exception is not handled by your application code, it is caught by this layer, which then automatically sends an appropriate user-friendly response.
    ![](https://docs.nestjs.com/assets/Filter_1.png)
    > 

    `cats.controller.ts`
    ```typescript
    @Get()
    async findAll() {
      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        error: 'This is a custom message',
      }, HttpStatus.FORBIDDEN);
    }
    ```

    * ### [Pipes](https://docs.nestjs.com/pipes)
    > Pipes in Nestjs are used to operate on the arguments of the controller route handler. This gives them two typical use cases:
    >
    > transformation —  transform input data to the desired output validation —  evaluate if the input data is valid
    Pipes can be created by implementing the PipeTransform interface on our class and overriding the transform function. Let’s look at a simple example 
    of a custom ValidationPipe:

    `custom.validation.pipe.ts`
    ```typescript
    import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

    @Injectable()
    export class CustomValidationPipe implements PipeTransform {
        transform(value: any, metadata: ArgumentMetadata) {
            const { metatype } = metadata;
            if (!metatype) {
                return value;
            }
            const convertedValue = plainToClass(metatype, value);
        
            return convertedValue;
        }
    }
    ```
    > In this example we check if the metatag we provided isn’t empty and if so we converted the received data to the metatype we defined.

    * ### [Guards](https://docs.nestjs.com/guards)
    > A guard is a class annotated with the @Injectable() decorator. Guards should implement the CanActivate interface.
    ![](https://docs.nestjs.com/assets/Guards_1.png)
    
    `auth.guard.ts`
    ```TypeScript
    import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
    import { Observable } from 'rxjs';

    @Injectable()
    export class AuthGuard implements CanActivate {
      canActivate(
        context: ExecutionContext,
      ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        return validateRequest(request);
      }
    }
    ```

    * ### [Interceptors](https://docs.nestjs.com/interceptors)
    > An interceptor is a class annotated with the @Injectable() decorator. 
    Interceptors should implement the NestInterceptor interface.
    ![](https://docs.nestjs.com/assets/Interceptors_1.png)
    
    > Interceptors have a set of useful capabilities which are inspired by the Aspect Oriented Programming (AOP) technique. They make it possible to:
    - bind extra logic before / after method execution
    - transform the result returned from a function
    - transform the exception thrown from a function
    - extend the basic function behavior
    - completely override a function depending on specific conditions (e.g., for caching purposes)

    * ### [Custom decorators](https://docs.nestjs.com/custom-decorators)
    > Nest is built around a language feature called decorators. Decorators are a well-known concept in a lot of commonly used programming languages, but in 
    the JavaScript world, they're still relatively new

    > In order to make your code more readable and transparent, you can create a 
    `@User()` decorator and reuse it across all of your controllers.

    `user.decorator.ts`
    ```typescript
    import { createParamDecorator, ExecutionContext } from '@nestjs/common';

    export const User = createParamDecorator(
      (data: unknown, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
      },
    );
    ```
    > Then, you can simply use it wherever it fits your requirements.
    
    `cats.controller.ts`
    ```typescript
    @Get()
    async findOne(@User() user: UserEntity) {
      console.log(user);
    }
    ```

- ## Fundamentals
    > You can follow next links to know more about fundamentals in NestJS
    - [Custom providers](https://docs.nestjs.com/fundamentals/custom-providers)
    - [Custom providers](https://docs.nestjs.com/fundamentals/custom-providers)
    - [Asynchronous providers](https://docs.nestjs.com/fundamentals/async-providers)
    - [Dynamic modules](https://docs.nestjs.com/fundamentals/dynamic-modules)
    - [Injection scopes](https://docs.nestjs.com/fundamentals/injection-scopes)
    - [Circular dependency](https://docs.nestjs.com/fundamentals/circular-dependency)
    - [Module reference](https://docs.nestjs.com/fundamentals/module-ref)
    - [Execution context](https://docs.nestjs.com/fundamentals/execution-context)
    - [Lifecycle events](https://docs.nestjs.com/fundamentals/lifecycle-events)
    - [Platform agnosticism](https://docs.nestjs.com/fundamentals/platform-agnosticism)
    - [Testing](https://docs.nestjs.com/fundamentals/testing)


- ## Testing
    > Nestjs provides us with a full setup of the Jest testing framework 
    which makes it easy to get started with unit, integration and 
    end-to-end tests.
    > 
    > Before you start testing I would recommend being familiar with the 
    testing pyramid and other best practices like the KISS (Keep it simple 
    stupid) technique.
    You can find information about testing after following [this link](https://docs.nestjs.com/fundamentals/testing).