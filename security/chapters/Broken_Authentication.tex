\subsection{Broken Authentication}

Attackers have access to hundreds of millions of valid username and password combinations for credential stuffing, default administrative account lists, automated brute force, and dictionary attack tools. Session management attacks are well understood, particularly in relation to unexpired session tokens.  

The prevalence of broken authentication is widespread due to the design and implementation of most identity and access controls. Session management is the bedrock of authentication and access controls, and is present in all stateful applications. Attackers can detect broken authentication using manual means and exploit them using automated tools with password lists and dictionary attacks. 

Attackers have to gain access to only a few accounts, or just one admin account to compromise the system. Depending on the domain of the application, this may allow money laundering, social security fraud, and identity theft, or disclose legally protected highly sensitive information. 


\subsubsection{Is the Application Vulnerable?}

Confirmation of the user's identity, authentication, and session management are critical to protect against authentication-related attacks.

There may be authentication weaknesses if the application:

\begin{itemize}
\item Permits automated attacks such as \href{https://www.owasp.org/index.php/Credential_stuffing}{credential stuffing}, where the attacker has a list of valid usernames and passwords.
\item Permits brute force or other automated attacks.
\item Permits default, weak, or well-known passwords, such as "Password1" or "admin/admin“.
\item Uses weak or ineffective credential recovery and forgotpassword processes, such as "knowledge-based answers", which cannot be made safe.
\item Uses plain text, encrypted, or weakly hashed passwords (see \autoref{SensitiveDataExposure}).
\item Has missing or ineffective multi-factor authentication.
\item Exposes Session IDs in the URL (e.g., URL rewriting).
\item Does not rotate Session IDs after successful login.
\item Does not properly invalidate Session IDs. User sessions or authentication tokens (particularly single sign-on (SSO) tokens) aren’t properly invalidated during logout or a period of inactivity.
\end{itemize}


\subsubsection{How to Prevent}

\begin{itemize}
\item Where possible, implement multi-factor authentication to prevent automated, credential stuffing, brute force, and stolen credential re-use attacks.
\item Do not ship or deploy with any default credentials, particularly for admin users.
\item Implement weak-password checks, such as testing new or changed passwords against a list of the \href{https://github.com/danielmiessler/SecLists/tree/master/Passwords}{top 10000 worst passwords}.
\item Align password length, complexity and rotation policies with \href{https://pages.nist.gov/800-63-3/sp800-63b.html#memsecret}{NIST 800-63 B's guidelines in section for Memorized Secrets} or other modern, evidence based password policies.
\item Ensure registration, credential recovery, and API pathways are hardened against account enumeration attacks by using the same messages for all outcomes.
\item Limit or increasingly delay failed login attempts. Log all failures and alert administrators when credential stuffing, brute force, or other attacks are detected.
\item Use a server-side, secure, built-in session manager that generates a new random session ID with high entropy after login. Session IDs should not be in the URL, be securely stored and invalidated after logout, idle, and absolute timeouts.
\end{itemize}


\subsubsection{Example Attack Scenarios}

Scenario 1: \href{https://www.owasp.org/index.php/Credential_stuffing}{Credential stuffing}, the use of \href{https://github.com/danielmiessler/SecLists}{lists of known passwords}, is a common attack. If an application does not implement automated threat or credential stuffing protections, the application can be used as a password oracle to determine if the credentials are valid.
\newline
\newline
Scenario 2: Most authentication attacks occur due to the continued use of passwords as a sole factor. Once considered best practices, password rotation and complexity requirements are viewed as encouraging users to use, and reuse, weak passwords. Organizations are recommended to stop these practices per NIST 800-63 and use multi-factor authentication.
\newline
\newline
Scenario 3: Application session timeouts aren’t set properly. A user uses a public computer to access an application. Instead of selecting “logout” the user simply closes the browser tab and walks away. An attacker uses the same browser an hour later, and the user is still authenticated. 

