\subsection{Insufficient Logging & Monitoring}

Exploitation of insufficient logging and monitoring is the bedrock of nearly every major incident.

Attackers rely on the lack of monitoring and timely response to achieve their goals without being detected.

This issue is included in the Top 10 based on an industry survey.

One strategy for determining if you have sufficient monitoring is to examine the logs following penetration testing. The testers’ actions should be recorded sufficiently to understand what damages they may have inflicted.

Most successful attacks start with vulnerability probing. Allowing such probes to continue can raise the likelihood of successful exploit to nearly 100%.

In 2016, identifying a breach took an average of 191 days – plenty of time for damage to be inflicted.

\subsubsection{Is the Application Vulnerable?}

Insufficient logging, detection, monitoring and active response occurs any time:

\begin{itemize}
\item Auditable events, such as logins, failed logins, and high-value transactions are not logged.
\item Warnings and errors generate no, inadequate, or unclear log messages.
\item Logs of applications and APIs are not monitored for suspicious activity.
\item Logs are only stored locally. 
\item Appropriate alerting thresholds and response escalation processes are not in place or effective.
\item Penetration testing and scans by \href{https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project}{DAST} tools (such as \href{https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project}{OWASP ZAP}) do not trigger alerts.
\item The application is unable to detect, escalate, or alert for active attacks in real time or near real time. You are vulnerable to information leakage if you make logging and alerting events visible to a user or an attacker (see \autoref{SensitiveDataExposure}).
\end{itemize}


\subsubsection{How to Prevent}

As per the risk of the data stored or processed by the application:

\begin{itemize}
\item Ensure all login, access control failures, and server-side input validation failures can be logged with sufficient user context to identify suspicious or malicious accounts, and held for sufficient time to allow delayed forensic analysis.
\item Ensure that logs are generated in a format that can be easily consumed by a centralized log management solutions.
\item Ensure high-value transactions have an audit trail with integrity controls to prevent tampering or deletion, such as append-only database tables or similar.
\item Establish effective monitoring and alerting such that suspicious activities are detected and responded to in a timely fashion.
\item Establish or adopt an incident response and recovery plan, such as \href{https://csrc.nist.gov/publications/detail/sp/800-61/rev-2/final}{NIST 800-61 rev 2} or later.
\end{itemize}


\subsubsection{Example Attack Scenarios}

Scenario 1: An open source project forum software run by a small team was hacked using a flaw in its software. The attackers managed to wipe out the internal source code
repository containing the next version, and all of the forum contents. Although source could be recovered, the lack of monitoring, logging or alerting led to a far worse breach. The forum software project is no longer active as a result of this issue.
\newline
\newline
Scenario 2: An attacker uses scans for users using a common password. They can take over all accounts using this password. For all other users, this scan leaves only one false login behind. After some days, this may be repeated with a different password.
\newline
\newline
Scenario 3: A major US retailer reportedly had an internal malware analysis sandbox analyzing attachments. The sandbox software had detected potentially unwanted software, but no one responded to this detection. The sandbox had been producing warnings for some time before the breach was detected due to fraudulent card transactions by an external bank.