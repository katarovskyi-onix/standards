\subsection{XML External Entities}

Attackers can exploit vulnerable XML processors if they can upload XML or include hostile content in an XML document, exploiting vulnerable code, dependencies or integrations. 

By default, many older XML processors allow specification of an external entity, a URI that is dereferenced and evaluated during XML processing. \href{https://www.owasp.org/index.php/Source_Code_Analysis_Tools}{SAST} tools can discover this issue by inspecting dependencies and configuration. \href{https://www.owasp.org/index.php/Category:Vulnerability_Scanning_Tools}{DAST} tools require additional manual steps to detect and exploit this issue. Manual testers need to be trained in how to test for XXE, as it not commonly tested as of 2017. 

These flaws can be used to extract data, execute a remote request from the server, scan internal systems, perform a denial-of-service attack, as well as execute other attacks. The business impact depends on the protection needs of all affected application and data. 


\subsubsection{Is the Application Vulnerable?}

Applications and in particular XML-based web services or
downstream integrations might be vulnerable to attack if:

\begin{itemize}
\item The application accepts XML directly or XML uploads, especially from untrusted sources, or inserts untrusted data into XML documents, which is then parsed by an XML rocessor.
\item Any of the XML processors in the application or SOAP based web services has \href{https://www.w3schools.com/xml/xml_dtd_intro.asp}{document type definitions (DTDs)} enabled. As the exact mechanism for disabling DTD processing varies by processor, it is good practice to consult a reference such as the \href{https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.md}{OWASP Cheat Sheet 'XXE Prevention’}.
\item If your application uses SAML for identity processing within federated security or single sign on (SSO) purposes. SAML uses XML for identity assertions, and may be vulnerable.
\item If the application uses SOAP prior to version 1.2, it is likely susceptible to XXE attacks if XML entities are being passed to the SOAP framework.
\item Being vulnerable to XXE attacks likely means that the application is vulnerable to denial of service attacks including the Billion Laughs attack.
\end{itemize}


\subsubsection{How to Prevent}

Developer training is essential to identify and mitigate XXE.
Besides that, preventing XXE requires:

\begin{itemize}
\item Whenever possible, use less complex data formats such as JSON, and avoiding serialization of sensitive data.
\item Patch or upgrade all XML processors and libraries in use by the application or on the underlying operating system. Use dependency checkers. Update SOAP to SOAP 1.2 or higher.
\item Disable XML external entity and DTD processing in all XML parsers in the application, as per the OWASP Cheat Sheet 'XXE Prevention'.
\item Implement positive ("whitelisting") server-side input validation, filtering, or sanitization to prevent hostile data within XML documents, headers, or nodes.
\item Verify that XML or XSL file upload functionality validates incoming XML using XSD validation or similar.
\item SAST tools can help detect XXE in source code, although manual code review is the best alternative in large, complex applications with many integrations. If these controls are not possible, consider using virtual patching, API security gateways, or Web Application Firewalls (WAFs) to detect, monitor, and block XXE attacks.
\end{itemize}


\subsubsection{Example Attack Scenarios}

Numerous public XXE issues have been discovered, including attacking embedded devices. XXE occurs in a lot of unexpected places, including deeply nested dependencies. The easiest way is to upload a malicious XML file, if accepted:

Scenario 1: The attacker attempts to extract data from the server:

\begin{lstlisting}[language=XML]
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE foo [
<!ELEMENT foo ANY >
<!ENTITY xxe SYSTEM "file:///etc/passwd" >]>
<foo>&xxe;</foo>
\end{lstlisting}


Scenario 2: An attacker probes the server's private network by changing the above ENTITY line to:

\begin{lstlisting}[language=XML]
<!ENTITY xxe SYSTEM "https://192.168.1.1/private" >]>
Scenario #3: An attacker attempts a denial-of-service attack by
including a potentially endless file:
<!ENTITY xxe SYSTEM "file:///dev/random" >]>
\end{lstlisting}