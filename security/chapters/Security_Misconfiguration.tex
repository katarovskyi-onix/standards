\subsection{Security Misconfiguration}
\label{SecurityMisconfiguration}

Attackers will often attempt to exploit unpatched flaws or access default accounts, unused pages, unprotected files and directories, etc to gain unauthorized access or knowledge of the system.

Security misconfiguration can happen at any level of an application stack, including the network services, platform, web server, application server, database, frameworks, custom code, and pre-installed virtual machines, containers, or storage. Automated scanners are useful for detecting misconfigurations, use of default accounts or configurations, unnecessary services, legacy options, etc.

Such flaws frequently give attackers unauthorized access to some system data or functionality. Occasionally, such flaws result in a complete system compromise. The business impact depends on the protection needs of the application and data.


\subsubsection{Is the Application Vulnerable?}

The application might be vulnerable if the application is:

\begin{itemize}
\item Missing appropriate security hardening across any part of the application stack, or improperly configured permissions on cloud services.
\item Unnecessary features are enabled or installed (e.g. unnecessary ports, services, pages, accounts, or privileges). 
\item Default accounts and their passwords still enabled and unchanged.
\item Error handling reveals stack traces or other overly informative error messages to users.
\item For upgraded systems, latest security features are disabled or not configured securely.
\item The security settings in the application servers, application frameworks (e.g. Struts, Spring, ASP.NET), libraries, databases, etc. not set to secure values.
\item The server does not send security headers or directives or they are not set to secure values.
\item The software is out of date or vulnerable (see \autoref{ComponentsWithKnownVulnerabilities}).
\end{itemize}

Without a concerted, repeatable application security configuration process, systems are at a higher risk.


\subsubsection{How to Prevent}

Secure installation processes should be implemented, including:
\begin{itemize}
\item A repeatable hardening process that makes it fast and easy to deploy another environment that is properly locked down. Development, QA, and production environments should all be configured identically, with different credentials used in each environment. This process should be automated to minimize the effort required to setup a new secure environment.
\item A minimal platform without any unnecessary features, components, documentation, and samples. Remove or do not install unused features and frameworks.
\item A task to review and update the configurations appropriate to all security notes, updates and patches as part of the patch management process (see \autoref{ComponentsWithKnownVulnerabilities}). In particular, review cloud storage permissions (e.g. S3 bucket permissions).
\item A segmented application architecture that provides effective, secure separation between components or tenants, with segmentation, containerization, or cloud security groups.
\item Sending security directives to clients, e.g. \href{https://www.owasp.org/index.php/OWASP_Secure_Headers_Project}{Security Headers}.
\item An automated process to verify the effectiveness of the configurations and settings in all environments.
\end{itemize}


\subsubsection{Example Attack Scenarios}

Scenario 1: The application server comes with sample applications that are not removed from the production server. These sample applications have known security flaws attackers use to compromise the server. If one of these applications is the admin console, and default accounts weren’t changed the attacker logs in with default passwords and takes over. 
\newline
\newline
Scenario 2: Directory listing is not disabled on the server. An attacker discovers they can simply list directories. The attacker finds and downloads the compiled Java classes, which they decompile and reverse engineer to view the code. The attacker then finds a serious access control flaw in the application. 
\newline
\newline
Scenario 3: The application server’s configuration allows detailed error messages, e.g. stack traces, to be returned to users. This potentially exposes sensitive information or underlying flaws such as component versions that are known to be vulnerable. 
\newline
\newline
Scenario 4: A cloud service provider has default sharing permissions open to the Internet by other CSP users. This allows sensitive data stored within cloud storage to be accessed 