\subsection{Broken Access Control}

Exploitation of access control is a core skill of attackers. \href{https://www.owasp.org/index.php/Source_Code_Analysis_Tools}{SAST} and \href{https://www.owasp.org/index.php/Category:Vulnerability_Scanning_Tools}{DAST} tools can detect the absence of access control but cannot verify if it is functional when it is present. Access control is detectable using manual means, or possibly through automation for the absence of access controls in certain frameworks.

Access control weaknesses are common due to the lack of automated detection, and lack of effective functional testing by application developers.
Access control detection is not typically amenable to automated static or dynamic testing. Manual testing is the best way to detect missing or ineffective access control, including HTTP method (GET vs PUT, etc), controller, direct object references, etc.

The technical impact is attackers acting as users or administrators, or users using privileged functions, or creating, accessing, updating or deleting every record.
The business impact depends on the protection needs of the application and data.

\subsubsection{Is the Application Vulnerable?}

Access control enforces policy such that users cannot act outside of their intended permissions. Failures typically lead to unauthorized information disclosure, modification or destruction of all data, or performing a business function outside of the limits of the user. Common access control vulnerabilities include:

\begin{itemize}
\item Bypassing access control checks by modifying the URL, internal application state, or the HTML page, or simply using a custom API attack tool.
\item Allowing the primary key to be changed to another users record, permitting viewing or editing someone else's account.
\item Elevation of privilege. Acting as a user without being logged in, or acting as an admin when logged in as a user.
\item Metadata manipulation, such as replaying or tampering with a JSON Web Token (JWT) access control token or a cookie or hidden field manipulated to elevate privileges, or abusing JWT invalidation
\item CORS misconfiguration allows unauthorized API access.
\item Force browsing to authenticated pages as an unauthenticated user or to privileged pages as a standard user. Accessing API with missing access controls for POST, PUT and DELETE.
\end{itemize}

\subsubsection{How to Prevent}

Access control is only effective if enforced in trusted server-side
code or server-less API, where the attacker cannot modify the
access control check or metadata.

\begin{itemize}
\item With the exception of public resources, deny by default.
\item Implement access control mechanisms once and re-use them throughout the application, including minimizing CORS usage.
\item Model access controls should enforce record ownership, rather than accepting that the user can create, read, update, or delete any record.
\item Unique application business limit requirements should be enforced by domain models.
\item Disable web server directory listing and ensure file metadata (e.g. .git) and backup files are not present within web roots.
\item Log access control failures, alert admins when appropriate (e.g. repeated failures).
\item Rate limit API and controller access to minimize the harm from automated attack tooling.
\item JWT tokens should be invalidated on the server after logout. Developers and QA staff should include functional access control unit and integration tests.
\end{itemize}


\subsubsection{Example Attack Scenarios}

Scenario 1: The application uses unverified data in a SQL call that is accessing account information:

\begin{lstlisting}[language=Java]
pstmt.setString(1, request.getParameter("acct"));
ResultSet results = pstmt.executeQuery( );
\end{lstlisting}

An attacker simply modifies the 'acct' parameter in the browser to send whatever account number they want. If not properly verified, the attacker can access any user's account.

\begin{verbatim} 
http://example.com/app/accountInfo?acct=notmyacct
\end{verbatim} 

Scenario 2: An attacker simply force browses to target URLs. Admin rights are required for access to the admin page.

\begin{verbatim} 
http://example.com/app/getappInfo
http://example.com/app/admin_getappInfo
\end{verbatim} 

If an unauthenticated user can access either page, it’s a flaw. If a non-admin can access the admin page, this is a flaw.
