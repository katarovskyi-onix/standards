# Onix Standards

The document include standard practices and recommendationf for development in different programming languages and frameworks. 

Strictly recommended contribute and follow those practices. We are building those standards as common language that helps read and understand us code of each other.

List of existing standards:


 - [Standards and recommended practices for development in the React](https://bitbucket.org/onix-systems/standards/src/master/react/standards-react.pdf)
 - [Standards and recommended practices for development in the Angular](https://bitbucket.org/onix-systems/standards/src/master/angular/standards-angular.pdf)
 - [Standards and recommended practices for development in the Vue](https://bitbucket.org/onix-systems/standards/src/master/vue/standards-vue.pdf)
 - [Standards and recommended practices for development in the Java](https://bitbucket.org/onix-systems/standards/src/master/java/standards-java.pdf)
 - [Standards and recommended practices for development in the Android](https://bitbucket.org/onix-systems/standards/src/master/android/standards-android.pdf)
 - [Standards and recommended practices for development in the iOS](https://bitbucket.org/onix-systems/standards/src/master/ios/standards-ios.pdf)
 - [Standards and recommended practices for development in HTML and CSS](https://bitbucket.org/onix-systems/standards/src/master/frontend/standards-frontend.pdf)
 - [Standards and recommended practices for Wordpress development](https://bitbucket.org/onix-systems/standards/src/master/wordpress/standards-wordpress.pdf)
 - [Security standards and recommended practices](https://bitbucket.org/onix-systems/standards/src/master/security/standards-security.pdf)
 - [Quality Asurance standards](https://bitbucket.org/onix-systems/standards/src/master/qa/standards-qa.pdf)
 - [Recommended practices for work on System Requirements Specification](https://bitbucket.org/onix-systems/standards/src/master/srs/system-requirement-specification.md)
 
 

Documents are created in LaTeX. There is no ideal tool for creating nice PDF documents but we have to admit, [Donald Knuth and Leslie Lamport did a great job](https://increment.com/open-source/the-lingua-franca-of-latex/).

We recommend to use [miktex](https://miktex.org/download) for editing and compiling documents locally. 
If you would like use some online tool for collaboration, great service is [overleaf.com](https://www.overleaf.com/).
Don't commit pdf in repository, they are compiled in repository automatically.

Please, feel free contribute those document.

All pdf files are building automatically. Details are in bitbucket-pipelines.yml file. All you need is just update according .tex files.

