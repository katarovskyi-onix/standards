New and old customers are contacting us daily with new requirements. The structure of these requirements, has many similar parts.
Following one structure repeated from project to project, will help all of us interact more efficiently, switch between projects, and speak the same language with our customers. 

Having researched a large number of project, and we tried to combine functional and business requirements into one simple universal structure, which with minor changes can be used in web and mobile projects. 

We encourage everyone to be actively involved in improving the proposed structure by proposing changes to the pull-requests to the document. 

Below is the recommended structure of requirements for development. Based on our experience it fits for most projects. The structure is simple and clear to all who are involved in the project. Depending on the type of project, some parts can be removed or extended by screen-shots, charts, diagrams.


## System Requirement Specification (ver 0.0.1)


### Roles

The section includes a list of roles in the application or system, and the actions allowed for each role.

An example:

 * Admin (disable user, send invite)
 * Provider (add/edit/enable/disable products)
 * Buyer (buy product)


### Screens

A section that includes pages/screens, with information displayed there and actions available on each screen.

An example:

#### Product page

*Display*

 * product picture
 * description
 * price
 

*Actions*

 * Add To Cart (add product in cart, display amount of products and total price in header)


### Data Scheme

The section includes the data structure with which the system operates and how they are interconnected.

An example:


#### User

 * first_name
 * last_name
 * email
 * password


#### Order
 
 * user_id
 * order_number
 * product_id
 * quantity
 * price
 * datetime

